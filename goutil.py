
# logical constants
GO_DIMENSION = 5 # how many cells in a row/col
GO_DIMENSION_EFFECTIVE = 2+GO_DIMENSION # effective dimension, including the extra 2 rows/cols
GO_CELLS_AMOUNT = GO_DIMENSION*GO_DIMENSION
GO_CELLS_AMOUNT_EFFECTIVE = GO_DIMENSION_EFFECTIVE*GO_DIMENSION_EFFECTIVE # including the edges which do not count on the real board

GO_POINT_BLACK = 41
GO_POINT_WHITE = 19
GO_POINT_NONE = 1 # inside the border but with no stone on it
GO_POINT_OOB = 0 # Out Of Border

# graphical constants
GO_CELL_SIZE = 60
GO_CELL_START = GO_CELL_START_X, GO_CELL_START_Y = GO_CELL_SIZE*0.8, GO_CELL_SIZE*0.8
GO_CELL_STONE_RATIO = 0.7
GO_STONE_RADIUS = GO_CELL_SIZE/2*GO_CELL_STONE_RATIO

GO_ALPHA_DUMMY = 140

QT_MAX_ALPHA = 255


# general utility methods

def go2index(pos): # go coordinates (1..N, 1..N) to board index 1..N*N
    return GO_DIMENSION_EFFECTIVE\
           +(pos[0])\
           +(GO_DIMENSION-pos[1])*GO_DIMENSION_EFFECTIVE

def index2go(i): # opposite of go2index
    return (i%GO_DIMENSION_EFFECTIVE,
            -1+GO_DIMENSION_EFFECTIVE-int(i/GO_DIMENSION_EFFECTIVE))


'''
Get a graphical point (x,y) and return board indices (i,j) of the closest logical point on the board.
Return None if point is out of bound
'''
def getClosestPoint(pos_x, pos_y):
    # check if mouse is near a point
    div_x = (pos_x - GO_CELL_START_X + GO_CELL_SIZE/2) / GO_CELL_SIZE
    div_y = (pos_y - GO_CELL_START_Y + GO_CELL_SIZE/2) / GO_CELL_SIZE

    pos_indices = (int(div_x)+1, GO_DIMENSION-int(div_y))

    if not (1 <= pos_indices[0] <= GO_DIMENSION) or not (1 <= pos_indices[1] <= GO_DIMENSION):
        return None # out of borders

    return pos_indices