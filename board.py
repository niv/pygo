#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
from gopoint import *
from goutil import *
from golegal import *


'''
The board itself represented in both logically and graphically way in self.board array:
- index represents normal computer science index of double-array, values range 0..N-1
- value at give index 'i' is a GoPoint(..) with graphical position and Go tuple representing a valid Go coordinates, from (1,1) to (N,N)
- the value also contains optional 'color' argument, which if None, means currently no stone is at that given position
- dummy points are stones which are not placed in the actual game and will be drawn as transparent (controlled by mouse movement event)

Empty board:
# # # # # # #
# - - - - - #
# - - - - - #
# - - - - - #
# - - - - - #
# - - - - - #
# # # # # # #

Board with some stones on it, black is on (1,3):
# # # # # # #
# - - - w - #
# - - w - w #
# b - - w - #
# - - - - - #
# - - - - - #
# # # # # # #


'''
class Board(QWidget):

    #region CTOR

    def __init__(self, parent=None):
        super(Board, self).__init__(parent)

        self.move_history = [] # list of tuples (i, color, []) in this case 'i' is simply an index of self.board, and the list is a list of removed indices due to that move

        self.go_lines = [] # graphical lines, basically only used for drawing

        self.points = [] # each index corosponding to a GoPoint of the board

        self.construct() # construct board and lines

        self.setMouseTracking(True) # tracking all mouse movments

        self.prev_hover = None # used for mouse movment "hovering" a valid point on board, giving a visual indication of where the stone will be placed


    #endregion

    # region Painting methods

    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        self.draw(qp)
        qp.end()

    def construct(self):
        self.points = [None] * GO_CELLS_AMOUNT_EFFECTIVE

        N = GO_DIMENSION_EFFECTIVE

        # construct logical points both real and out of border
        for i in range(N):
            for j in range(N):
                index = i*N+j
                self.points[index] = GoPoint(index)
                if i is 0 or j is 0 or i is N-1 or j is N-1:
                    self.points[index].value = GO_POINT_OOB


        # construct graphical lines and adding graphical positions for points in array
        end_y = GO_CELL_START_Y+(GO_DIMENSION-1)*GO_CELL_SIZE
        end_x = GO_CELL_START_X+(GO_DIMENSION-1)*GO_CELL_SIZE
        point_index = N+1 # the starting point of actual game point
        for start_y in [GO_CELL_START_Y + y*GO_CELL_SIZE for y in range(GO_DIMENSION)]:
            self.go_lines.append( (GO_CELL_START_X, start_y, end_x, start_y) )
            for start_x in [GO_CELL_START_X + x*GO_CELL_SIZE for x in range(GO_DIMENSION)]:
                self.points[point_index].gpos = (start_x, start_y)
                self.go_lines.append( (start_x, start_y, start_x, end_y) )
                point_index += 1
            point_index += 2


    def draw(self, qp):
        pen = QPen(Qt.black, 2, Qt.SolidLine)


        qp.setBrush(QColor(220, 220, 150))
        qp.drawRect(0, 0, \
                    2*GO_CELL_START_X+(GO_DIMENSION-1)*GO_CELL_SIZE, 2*GO_CELL_START_Y+(GO_DIMENSION-1)*GO_CELL_SIZE)

        qp.setPen(pen)

        for line in self.go_lines:
            qp.drawLine(*line)

        for point in self.points:
            point.draw(qp)


    #endregion

    # region Stone manipulation

    def addStone(self, color, pos, dummy = False):

        point = self.points[go2index(pos)]
        if point.value is GO_POINT_NONE:
            point.update(color, dummy)
            return True
        else:
            return False

    def removeStone(self, pos):
        self.points[go2index(pos)].update(GO_POINT_NONE)

    # endregion


    '''
    Paint a transparent stone on the closest point found graphically.
    Return True if repaint is necesary, False otherwise
    '''
    def paintTransparentStone(self, qpos, turn_color = GO_POINT_BLACK):
        to_repaint = False

        pos_indices = getClosestPoint(qpos.x(), qpos.y())

        if self.prev_hover:
            if self.prev_hover == pos_indices: # hovering on same position, no need to update anything
                return
            self.removeStone(self.prev_hover)
            self.prev_hover = None
            to_repaint = True

        if pos_indices:
            if self.addStone(turn_color, pos_indices, True):
                self.prev_hover = pos_indices # will only update previous hover if we added succesfully
            to_repaint = True

            print getEyeColor(self, pos_indices)

        return to_repaint


    '''
    Each mouse movement will be checked if hovering over valid point, and will give a visual indication
    of a transparent stone that could be put at the closest point.
    '''
    def mouseMoveEvent (self, q_mouseEvent):
        qpos = q_mouseEvent.pos()

        to_repaint = False

        to_repaint = to_repaint or self.paintTransparentStone(qpos)

        if to_repaint:
            self.repaint()


def test(w):
    board = Board(w)
    layout=QHBoxLayout()
    layout.addWidget(board)
    w.setLayout(layout)

    board.addStone(GO_POINT_BLACK, (1,3))
    board.addStone(GO_POINT_BLACK, (2,3))
    board.addStone(GO_POINT_BLACK, (2,2))
    board.addStone(GO_POINT_BLACK, (2,1))
    board.addStone(GO_POINT_BLACK, (1,1))

    board.addStone(GO_POINT_WHITE, (4,5))
    board.addStone(GO_POINT_WHITE, (5,4))
    board.addStone(GO_POINT_WHITE, (4,3))
    board.addStone(GO_POINT_WHITE, (3,4))



    #printPoints(board.points)
