#! /usr/bin/env python
# -*- coding: utf-8 -*-
# __author__ = 'Niv'

from board import *


WINDOW_SIZE = WINDOW_WIDTH, WINDOW_HEIGHT = 500, 400

class MainWindow(QWidget):

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        # Set window size.
        self.resize(*WINDOW_SIZE)

        # Set window title
        self.setWindowTitle("GO!")

if __name__ == '__main__':

    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()

    test(window)

    sys.exit(app.exec_())