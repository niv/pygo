#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
from gopoint import *
from goutil import *
from board import *


'''
If a given position is considered a Go-eye, return its color. otherweise return None
A valid eye is if an empty square is surrounded (north,east,south,west) by stones of same color.
'''
def getEyeColor(gb, ipos):
    point = gb.points[go2index(ipos)]
    #print 'check eye',ipos,go2index(ipos),point.ineighbors

    # must be empty to be considered an eye
    if not point.isEmpty():
        return None

    eye_color = None

    neighbors_sum = sum([gb.points[i].value for i in point.ineighbors])
    #print neighbors_sum

    if 0 is neighbors_sum % GO_POINT_BLACK:
        return GO_POINT_BLACK
    if 0 is neighbors_sum % GO_POINT_WHITE:
        return GO_POINT_WHITE

    return None

    '''
    for ineighbor in point.ineighbors:
        if ineighbor is None: # if no neighbour we're at edge of board, skip that neighbour
            continue
        # making sure they all same color, otherwise not an eye
        neighbor_color = gb.points[ineighbor].color
        if eye_color is None:
            eye_color = neighbor_color
        else:
            if eye_color is not neighbor_color:
                return None
    '''
    return eye_color

'''
Return True if its legal to put stone of given color at given ipos=(i,j), False otherwise
'''
def isLegalMove(gb, ipos, color):
    point = gb.points[go2index(ipos)]

    # (1) ipos must be empty of any stone
    if point.isEmpty():
        return False

    # (2)

