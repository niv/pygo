
import sys
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from threading import Thread
from time import sleep
from goutil import *

# graphical variables
go_stone_qtcolor = {GO_POINT_BLACK: QColor('black'), GO_POINT_WHITE: QColor('white')}


def printPoints(points):
    for i in range(GO_DIMENSION_EFFECTIVE):
        for j in range(GO_DIMENSION_EFFECTIVE):
            print points[i*GO_DIMENSION_EFFECTIVE+j],
        print
    print

"""
Represent a point in the GO board.
A point is one of:
1) Colored stone at given point position
2) Dummy colored stone at given position, that isn't really on the board thus will be transparent
3) Empty point, with no stone on it
4) Out of bound point, which is only used for faster/easier calculations regarding the board itself
"""
class GoPoint():
    def __init__(self, index, value = GO_POINT_NONE, dummy = None):
        self.value = value
        self.dummy = dummy # either None for the point not containing a dummy, or black/white value

        self.index = index # the actual index that will be in the board.points list. will be used for easier access of neighboring points
        self.ipos = index2go(index) # indecial position according to Go numbering (1,1)..(N,N)
        self.gpos = None # update graphical position manually

        self.ineighbors = (self.getNorth(), self.getEast(), self.getSouth(), self.getWest()) # north,east,souh,west

    def clear(self):
        self.update(GO_POINT_NONE)

    def update(self, value, dummy=None):
        self.value = value
        self.dummy = dummy

    def isEmpty(self):
        return self.dummy is not None or self.value is GO_POINT_NONE

# neighbor functions. those can actually be staticaclly calculated and saved for each point. so in theory, private methods

    def getNorth(self):
        return self.index - GO_DIMENSION_EFFECTIVE

    def getSouth(self):
        return self.index + GO_DIMENSION_EFFECTIVE

    def getEast(self):
        return self.index + 1

    def getWest(self):
        return self.index - 1

    '''
    Note: During draw, painter brush is being changed
    '''
    def draw(self, painter):
        value = self.value
        if value in go_stone_qtcolor:
            qt_color = go_stone_qtcolor[value]
            if self.dummy:
                qt_color.setAlpha(GO_ALPHA_DUMMY)
            else:
                qt_color.setAlpha(QT_MAX_ALPHA)
            painter.setBrush(qt_color)
            painter.drawEllipse(QPoint(*self.gpos), GO_STONE_RADIUS, GO_STONE_RADIUS)

    def __str__(self):
        if self.value is GO_POINT_BLACK: return 'b'
        if self.value is GO_POINT_WHITE: return 'w'
        if self.value is GO_POINT_OOB: return '#'
        return '-'